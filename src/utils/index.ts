import { useEffect, useState } from 'react'

/**
 * 将对象中key为空的属性删除
 * @param {Object} obj
 * @returns
 */
export const cleanObj = (obj: object) => {
  // 在一个函数里面，改变传入的对象本身是不好的
  const res = { ...obj }
  Object.keys(obj).forEach(key => {
    // @ts-ignore
    const val = obj[key]
    if (!isFalse(val)) {
      // 如果val为空，删除对应的属性
      // @ts-ignore
      delete res[key]
    }
  })
  return res
}


/**
 * 把undefined、null、空 等转换成布尔值 false
 * 0除外 (0是有意义的数字)
 * @param {*} v 
 * @returns boolean
 */
export const isFalse = (v: unknown) => v === 0 ? true : !!v

/**
 * 自定义hook 初始化的时候执行一次 componentDidMount
 * @param {function} callback
 */
export const useMount = (callback: () => void) => {
  useEffect(() => {
    callback()
  }, [])
  // 给它一个空数组就像 componentDidMount就像，它只运行一次。
  // 不给它第二个参数 充当 componentDidMount 和 componentDidUpdate
}

/**
 * 自定义hook 防抖
 * @param {*} value 需要加上防抖效果的变量 来限制该变量只在delay中最后一次变化发生改变
 * @param {number} delay 延迟时间
 */
export const useDebounce: <T>(value: T, delay?: number) => T = (value, delay) => {
  // 将value使用防抖来限制变化过快，只在delay内的最后一次变化才会发生修改
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    // 防抖
    // 这里会反复执行多次，只要value变化就会执行
    const time = setTimeout(() => {
      // 但是这里会执行很少的次数（delay中的最后一次变化）
      setDebouncedValue(value)
    }, delay)
    // 原因是每次刚创建一个setTimeout
    // 就在下面的return中被取消了（如果是在delay中变化的）
    console.log('执行！！！ :>> ', time);
    return () => {
      // 清除副作用
      console.log('清除了 :>> ', value);
      clearTimeout(time)
    }
  }, [value, delay])

  return debouncedValue
}


/**
 * 作业
 * 自定义hook
 * 实现数组的管理
 */
export const useArray = <T,>(obj: T[]) => {
  // 使用useState接受传入的参数
  const [handleObj, setHandleObj] = useState(obj)

  return {
    handleObj,
    setHandleObj,
    // add中的参数必须和参数数组中元素一样
    add: (item: T) => setHandleObj([
      ...handleObj,
      item
    ]),
    clear: () => setHandleObj([]),
    removeIndex: (index: number) => {
      const arr: T[] = [...handleObj]
      // 在index的位置上删除一个元素
      arr.splice(index,1)
      setHandleObj(arr)
    }
  }
}