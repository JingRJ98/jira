/**
 * 实现一个自定义HOOK useArray
 * 要求支持value,clear,removeIndex,add
 */
import { useArray } from "utils"

export const TestCustomHook = () => {
  const index = 1
  const persons: { name: string; age: number }[] = [
    { name: "孙悟空", age: 22 },
    { name: "唐三藏", age: 39 },
    { name: "沙和尚", age:50 },
  ]

  const {handleObj,clear,removeIndex,add} = useArray(persons)

  return (
    <div>
      {/* 期待：点击后增加猪八戒 */}
      <button onClick={() => add({name: "猪八戒",age: 40})}>add 猪八戒</button>
      {/* 期待：点击后删除第 index 项 */}
      <button onClick={() => removeIndex(index)}>删除第{index}项</button>
      {/* 期待：点击后清空全部项目 */}
      <button onClick={() => clear()}>删除数组中所有元素</button>
      {
        handleObj.map((p,i) => (
          <div>
            <span style={{color: 'red'}}>{i}</span>
            <span>{p.name}</span>
            <span>{p.age}</span>
          </div>
        ))
      }
    </div>
  )
}