import { useState, useEffect } from 'react';
import { SearchPanel } from './SearchPanel'
import { List } from './List'
import { cleanObj, useDebounce } from 'utils/index'
import qs from 'qs';

const apiURL: string | undefined = process.env.REACT_APP_API_URL
export const PorjectListScreen = () => {
  const [list, setList] = useState([])
  // 人员信息
  // 当search组件里面的选择栏或者输入框发生change就会执行set
  // 这里的param变化的很快（因为一旦输入框中发生change就会执行）
  const [param, setParam] = useState({
    name: '',
    personId: ''
  })
  // 将这个变量使用防抖来限制变化过快，只在delay内的最后一次变化才会发生修改
  const debouncedParam = useDebounce(param, 1000);
  const [users, setUsers] = useState([])
  console.log('users :>> ', users);

  useEffect(() => {
    fetch(`${apiURL}/projects?${qs.stringify(cleanObj(debouncedParam))}`).then(async res => {
      if (res.ok) {
        setList(await res.json())
      }
    })
    // 当这个防抖后的变量发生改变的时候才会执行发请求操作
  }, [debouncedParam])

  useEffect(() => {
    // 初始化users
    fetch(`${apiURL}/users`).then(async res => {
      if (res.ok) {
        setUsers(await res.json())
      }
    })
  }, [])
  return (
    <div>
      <SearchPanel
        param={param}
        setParam={setParam}
        users={users}
      ></SearchPanel>
      <List
        list={list}
        users={users}
      ></List>
    </div>
  )
}


