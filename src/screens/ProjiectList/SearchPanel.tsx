export interface IUser {
  id: string;
  name: string;
  email: string;
  title: string;
  organization: string;
}
interface ISearchPanelProps {
  users: IUser[];
  param: {
    name: string;
    personId: string;
  },
  setParam: (param: ISearchPanelProps['param']) => void
}
export const SearchPanel = (props: ISearchPanelProps) => {
  const { param, setParam, users } = props

  return (
    <form action="">
      <div>
        <input
          type="text"
          value={param.name}
          // 当输入框的内容发生change的时候就会调用set
          onChange={e => setParam({
            ...param,
            name: e.target.value
          })}
        />
        {/* 当选择栏的内容发生change的时候就会调用set */}
        <select value={param.personId} onChange={e => setParam({
          ...param,
          personId: e.target.value
        })}>
          <option value="">负责人</option>
          {
            users.map((u: any) => {
              return (
                <option value={u.id} key={u.id}>{u.name}</option>
              )
            })
          }
        </select>
      </div>
    </form>
  )
}