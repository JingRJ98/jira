import {IUser} from './SearchPanel'

interface IListProps {
  list: IProject[];
  users: IUser[];
}

interface IProject {
  id: string;
  name: string;
  personId: string;
  pin: boolean;
  organization: string;
}
export const List = (props: any) => {
  const { list, users } = props
  return (
    <table>
      <thead>
        <tr>
          <th>名称</th>
          <th>负责人</th>
        </tr>
      </thead>
      <tbody>
        {
          list.map((pro: any) => {
            return (
              <tr key={pro.id}>
                <td>{pro.name}</td>
                <td>{users.find((u: IUser) => u.id === pro.personId)?.name || '未知'}</td>
              </tr>
            )
          })
        }
      </tbody>
    </table>
  )
}